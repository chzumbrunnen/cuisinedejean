<?php
/**
 * The template used for displaying hero content in page.php and page-templates.
 *
 * @package Sela
 */
?>

<?php if ( has_post_thumbnail() ) : ?>
	<div class="hero">
		<div class="entry-thumbnail">
			<!-- no featured image ;-) -->
		</div>
	</div><!-- .hero -->
<?php endif; ?>