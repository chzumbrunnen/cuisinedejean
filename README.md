# README #

This repo contains a child theme of the WordPress theme [Sela](https://wordpress.org/themes/sela/) by automattic.
It's not meant to be a public project but is just the repository used to manage a client's WordPress theme.

It's no secret either. So it's public to make use of the excellent free version of [WP Pusher](https://wppusher.com/).

### What is this repository for? ###

* Using this repo makes it easy to change the theme to clients requirements
* The Push-to-Deploy feature of WP Pusher is a great way to deploy from local to production.

### How do I get set up? ###

* This is a child theme, so without its parent theme [Sela](https://wordpress.org/themes/sela/) it's not usable.
* WordPress actually loads a parent theme automatically on installation of a child theme. So theoretically it's possible to use this theme on any self hosted WordPress site.

### Main differences to the parent themes are: ###

* the header image is below the actual header
* using of site logo and/or title/sub-title above the header image
* 3 columns instead of 2 for grid-templates
* featured image is not shown on single pages

### Contribution guidelines ###

* It's not expected that anyone contributes, but improvements are always welcome. 

### Who do I talk to? ###

* visit me on [homepage-abc.ch](http://homepage-abc.ch/) (German) or contact me via Twitter @chzumbrunnen